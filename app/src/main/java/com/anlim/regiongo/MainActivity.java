package com.anlim.regiongo;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.anlim.regiongo.fragments.LastRegion;
import com.anlim.regiongo.fragments.RegionList;
import com.anlim.regiongo.fragments.SearchRegion;
import com.anlim.regiongo.logic.dbHelper;

public class MainActivity extends AppCompatActivity {

    Menu menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        dbHelper dbHelper = new dbHelper(this);
        Cursor cursor = dbHelper.checkBase();
        if(cursor.getCount() == 0)
            dbHelper.createTableFromJSON(this);

        loadFragment(LastRegion.newInstance());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.counter, menu);

        this.menu = menu;

        MenuItem item_conter = menu.findItem(R.id.counter_item);
        dbHelper dbHelper = new dbHelper(this);

        String count = String.valueOf(dbHelper.getCountRegions().getCount()) + "/86";
        item_conter.setTitle(count);

        return true;
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_last_region:
                    loadFragment(LastRegion.newInstance());
                    return true;
                case R.id.navigation_region_find:
                    loadFragment(SearchRegion.newInstance());
                    return true;
                case R.id.navigation_region_list:
                    loadFragment(RegionList.newInstance());
                    return true;
            }
            return false;
        }
    };

    public void loadFragment(Fragment fragment) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.frame, fragment);
        ft.commit();
    }

    public void updateCount(){
        dbHelper dbHelper = new dbHelper(this);
        MenuItem item_conter = menu.findItem(R.id.counter_item);
        String count = String.valueOf(dbHelper.getCountRegions().getCount()) + "/86";
        item_conter.setTitle(count);
    }
}
