package com.anlim.regiongo.fragments;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.anlim.regiongo.MainActivity;
import com.anlim.regiongo.R;
import com.anlim.regiongo.logic.dbHelper;

import java.util.ArrayList;

public class SearchRegion extends Fragment implements AdapterView.OnItemClickListener {

    GridView gvNotFound;
    ArrayAdapter<String> adapter;
    EditText etSearch;
    FloatingActionButton fabSearchRegion;

    public static SearchRegion newInstance() {
        return new SearchRegion();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)  {
        View rootView = inflater.inflate(R.layout.fragment_search_region, container, false);

        etSearch = rootView.findViewById(R.id.etSearch);
        dbHelper dbHelper = new dbHelper(getActivity());
        Cursor cursor = dbHelper.getNotFoundRegions();

        fabSearchRegion = rootView.findViewById(R.id.fabSearchRegion);
        fabSearchRegion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String result = etSearch.getText().toString().trim();

                if(!result.equals(""))
                    SearchInBase(result);
            }
        });

        ArrayList<String> data = new ArrayList<>();
        for (int i = 0; i < cursor.getCount(); i++) {
            cursor.moveToPosition(i);
            data.add(cursor.getString(0));
        }

        adapter = new ArrayAdapter<String>(getContext(), R.layout.item_not_found_region, R.id.tvNotFoundCode, data);

        gvNotFound = rootView.findViewById(R.id.gvNotFound);
        gvNotFound.setAdapter(adapter);
        gvNotFound.setOnItemClickListener(this);

        return rootView;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        SearchInBase(adapter.getItem(position));
    }

    private void SearchInBase(String result){
        int res = Integer.parseInt(result.replaceAll("[^0-9]", ""));

        dbHelper dbHelper = new dbHelper(getContext());
        int codeRegion = dbHelper.searchByNumber(res);
        if(codeRegion != 0){
            dbHelper.setResultFind(codeRegion);
            loadFragment();

            //Обновление количества найденных регионов
            MainActivity activity = (MainActivity) getContext();
            activity.updateCount();

        }else
            Toast.makeText(getContext(), "Нет совпадений", Toast.LENGTH_SHORT).show();
    }

    private void loadFragment() {
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.frame, LastRegion.newInstance());
        ft.commit();
    }
}
