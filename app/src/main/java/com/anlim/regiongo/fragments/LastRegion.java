package com.anlim.regiongo.fragments;
import android.content.Context;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.anlim.regiongo.R;
import com.anlim.regiongo.logic.dbHelper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class LastRegion extends Fragment {

    public static LastRegion newInstance() {
        return new LastRegion();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_last_region, container, false);

        ImageView ivLastRegion = rootView.findViewById(R.id.ivLastRegion);
        ImageView ivMapLastRegion = rootView.findViewById(R.id.ivMapRegion);
        TextView tvLastRegion = rootView.findViewById(R.id.viewerName);
        TextView tvNumberRegion = rootView.findViewById(R.id.tvNumberRegion);

        dbHelper dbHelper = new dbHelper(getActivity());
        Context context = getActivity();
        Cursor cursor = dbHelper.getLastRegion();

        if(cursor.getCount() != 0){
            cursor.moveToFirst();

            String itemNumber = cursor.getString(1);
            tvNumberRegion.setText(itemNumber);

            String iconFlag = "r"+ itemNumber;
            int drawable = context.getResources().getIdentifier(iconFlag, "drawable", context.getPackageName());
            ivLastRegion.setImageResource(drawable);

            String iconMap = "m"+ itemNumber;
            int drawableMap = context.getResources().getIdentifier(iconMap, "drawable", context.getPackageName());
            ivMapLastRegion.setImageResource(drawableMap);

            String itemName = cursor.getString(0);
            tvLastRegion.setText(itemName);

        } else {
            ivLastRegion.setImageResource(R.drawable.ic_question);
            tvLastRegion.setText("Это что за регион?");
            tvNumberRegion.setText("XX");
        }
        return rootView;
    }
}
