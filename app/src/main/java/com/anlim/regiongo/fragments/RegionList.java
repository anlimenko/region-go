package com.anlim.regiongo.fragments;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.anlim.regiongo.R;
import com.anlim.regiongo.logic.RecyclerViewAdapter;
import com.anlim.regiongo.logic.dbHelper;

public class RegionList extends Fragment {

    public static RegionList newInstance() {
        return new RegionList();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_region_list, container, false);

        dbHelper dbHelper = new dbHelper(getActivity());

        RecyclerView regionList = rootView.findViewById(R.id.recyclerView);
        regionList.setLayoutManager(new LinearLayoutManager(getActivity()));
        RecyclerViewAdapter adapter = new RecyclerViewAdapter(dbHelper.getRegionList());
        regionList.setAdapter(adapter);

        return rootView;
    }

}
