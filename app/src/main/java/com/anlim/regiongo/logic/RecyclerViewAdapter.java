package com.anlim.regiongo.logic;

import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.anlim.regiongo.R;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder> {
    private Cursor cursor;
    private Context context;

    public RecyclerViewAdapter(Cursor cursor){
        this.cursor = cursor;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull final ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.region_list_item, parent, false);
        context = parent.getContext();

        /*view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View item) {
                LayoutInflater layoutInflater = LayoutInflater.from(context);
                View dialogView = layoutInflater.inflate(R.layout.layout_viewer, null);

                ImageView viewerIcon = dialogView.findViewById(R.id.viewerIcon);
                TextView  viewerName = dialogView.findViewById(R.id.viewerName);
                TextView  viewerDate = dialogView.findViewById(R.id.viewerDate);

                viewerName.setText("TEXT");

                new AlertDialog.Builder(context)
                        .setView(dialogView)
                        .setPositiveButton("Закрыть",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int id) {
                                    }
                                }).create().show();
            }
        });*/
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        cursor.moveToPosition(position);

        String itemName = cursor.getString(0);
        String itemNumber = cursor.getString(1);
        int itemStatus = cursor.getInt(2);

        holder.mItemName.setText(itemName);
        if(itemStatus == 1){
            holder.mItemNumber.setVisibility(View.VISIBLE);
            holder.mItemNumber.setText(itemNumber);

            holder.mItemIcon.setVisibility(View.VISIBLE);
            String iconName = "rmr"+ itemNumber;
            int drawable = context.getResources().getIdentifier(iconName, "drawable", context.getPackageName());
            holder.mItemIcon.setImageResource(drawable);

        } else {
            holder.mItemNumber.setVisibility(View.GONE);
            holder.mItemIcon.setVisibility(View.GONE);
        }
    }

    static class MyViewHolder extends RecyclerView.ViewHolder{

        private TextView mItemName;
        private TextView mItemNumber;
        private ImageView mItemIcon;

        MyViewHolder(final View itemView) {
            super(itemView);

            mItemName = itemView.findViewById(R.id.itemName);
            mItemNumber = itemView.findViewById(R.id.itemNumber);
            mItemIcon = itemView.findViewById(R.id.itemIcon);
        }
    }

    @Override
    public int getItemCount() {
        return cursor.getCount();
    }
}
