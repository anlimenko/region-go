package com.anlim.regiongo.logic;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

public class dbHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "regiongo.db";
    private static final int DATABASE_VERSION = 1;

    public dbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS Districts(" +
                "_id INTEGER, " +
                "Code INTEGER, " +
                "Name TEXT, " +
                "SearchCode INTEGER, " +
                "Status INTEGER, " +
                "Date TEXT);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public Cursor checkBase(){
        SQLiteDatabase db = this.getReadableDatabase();
        return db.rawQuery("select Name" +
                " from Districts" +
                " LIMIT 1", null);
    }

    public void createTableFromJSON(Context context){

        String json = null;
        dbHelper database;
        SQLiteDatabase db;
        JSONArray array = null;


        try {
            InputStream is = context.getAssets().open("districts.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException e) {
            Toast.makeText(context, "Ошибка при создании строки JSON", Toast.LENGTH_SHORT).show();
        }

        try {
            JSONObject obj = new JSONObject(json);
            array = obj.getJSONArray("data");
        }  catch (JSONException e) {
            Toast.makeText(context, "Ошибка при создании массива JSON", Toast.LENGTH_SHORT).show();
        }

        try {
            database = new dbHelper(context);
        }
        catch (Exception e){
            Toast.makeText(context, "Ошибка получения экземпляра БД", Toast.LENGTH_SHORT).show();
            return;
        }

        try{
            db = database.getWritableDatabase();
        }
        catch (Exception e) {
            Toast.makeText(context, "Ошибка чтения БД", Toast.LENGTH_SHORT).show();
            return;
        }

        try{
            db.execSQL("DELETE from Districts");
        }catch (Exception e) {
            Toast.makeText(context, "Ошибка очистки таблицы БД", Toast.LENGTH_SHORT).show();
            return;
        }

        db.beginTransaction();
        ContentValues row = new ContentValues();
        try {
            for (int i = 0; i < array.length(); i++) {
                JSONObject elem = array.getJSONObject(i);

                Iterator<?> keys = elem.keys();
                while(keys.hasNext() ) {
                    String key = (String)keys.next();
                    row.put(key, elem.getString(key));
                }
                db.insert("Districts", null, row);
                row.clear();
            }

            db.setTransactionSuccessful();
        }
        catch (Exception e){
            Toast.makeText(context, "Ошибка вставки данных в таблицу", Toast.LENGTH_SHORT).show();
        }
        finally {
            db.endTransaction();
        }
    }

    public Cursor getRegionList(){
        SQLiteDatabase db = this.getReadableDatabase();
        return db.rawQuery("select distinct Name, Code, Status from Districts order by Status desc, Code", null);
    }

    public int searchByNumber(int number){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("select Code" +
                " from Districts" +
                " where SearchCode = " + number, null);

        if(cursor.getCount()>0){
            cursor.moveToFirst();
            return cursor.getInt(0);
        }

        cursor.close();
        db.close();
        return 0;
    }

    public void setResultFind(int number){
        Date today = new Date();
        SimpleDateFormat fmtShort = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = fmtShort.format(today);

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("update Districts set Status = 1, Date = '" + date +  "' where Code = " + number);
        db.close();
    }

    public Cursor getLastRegion(){
        SQLiteDatabase db = this.getReadableDatabase();
        return db.rawQuery("select Name, Code" +
                " from Districts" +
                " where Status = 1" +
                " order by Date desc" +
                " limit 1", null);
    }

    public Cursor getCountRegions(){
        SQLiteDatabase db = this.getReadableDatabase();
        return db.rawQuery("select distinct Code" +
                " from Districts" +
                " where Status = 1", null);
    }

    public Cursor getNotFoundRegions(){
        SQLiteDatabase db = this.getReadableDatabase();
        return db.rawQuery("select distinct Code from Districts where Status isnull order by Random() limit 12", null);
    }
}
